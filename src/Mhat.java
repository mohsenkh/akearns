import java.util.ArrayList;
import java.util.HashMap;


public class Mhat extends Binary 
{

	 public  static HashMap <String,String> FMTable =new HashMap  <String,String> ();
	 public  static HashMap <String,String> OUTMhat =new HashMap  <String,String> ();
	 
     	
	 public MooreAutomata HMhat(ArrayList<String> transition, ArrayList<String> prefixes)
	 {
                 tree mhatTree = new tree();
		 String B = "";
		 for (int j = 0;j < prefixes.size();j++)
		 {
	          for (int i = 0;i < transition.size();i++)
	           {
			         B = prefixes.get(j) + transition.get(i);
		                 OUTMhat.put(prefixes.get(j), FinalDFA(prefixes.get(j), randomooreAutomaton.initialState));
		                 String Transition = mhatTree.sift(B, tree.root);
		                 FMTable.put(B, Transition);

		  
		      } 
		 
		  }
		 
		  MooreAutomata getHypothesis = new MooreAutomata();
		  getHypothesis.setinitalState(initMhatState);
		  getHypothesis.setinputAlphabet(transition);
		  getHypothesis.setstateSet(prefixes);
		  getHypothesis.settransitionTable(FMTable);
		  getHypothesis.setlambdaMap(OUTMhat);
		  
	/*	  System.out.println("getHypothesis.initialState"+getHypothesis.initialState);
          System.out.println("getHypothesis.transitionTable"+getHypothesis.transitionTable);
          System.out.println("getHypothesis.lambdaMap"+getHypothesis.lambdaMap);*/
		
		  return getHypothesis; 
	 }


}	
	
	


