import java.util.ArrayList;
import java.util.HashMap;



public class MooreAutomata 
{
	
	String initialState;
	ArrayList <String> inputAlphabet;
	ArrayList<String> stateSet;
	public HashMap<String,String> transitionTable = new HashMap<String,String>(); 
	public HashMap<String,String> lambdaMap = new HashMap<String, String>();
	
	
	public MooreAutomata()
	{
	       inputAlphabet = new ArrayList <String> ();
	       stateSet = new ArrayList<String>();
	       transitionTable = new HashMap<String,String>();
	       lambdaMap= new HashMap<String,String>();
	}
	
	public void setinputAlphabet (ArrayList<String> input)
	{
		inputAlphabet.addAll(input);
	}
	
	public void setinitalState (String initinput)
	{
		initialState = initinput;
	}
	
	public void settransitionTable(HashMap<String,String> transitoninput)
	{
		transitionTable.putAll(transitoninput);
	}
	
	public void setstateSet(ArrayList<String> stateinput)
	{
		stateSet.addAll(stateinput);
	}
	
	public void setlambdaMap (HashMap<String,String> lambdainput)
	{
		lambdaMap.putAll(lambdainput);
	}

}
