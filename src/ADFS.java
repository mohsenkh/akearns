import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class ADFS extends Binary 
{
	
	public static boolean AA = true;
	public static boolean BB = true;
	public static boolean AAA = true;
	public static HashSet <String> visited1 = new HashSet <String>();
	public static HashMap <String,String> Partition = new HashMap <String,String>();
	// Advanced Depth First Search method.
	public static String DFS(String initM,String initMhat,String CE, ArrayList <String> visitedStates,MooreAutomata getHypothesis)
	{
		

		boolean vis = visitedStates.contains(initM);
                String CE_tmp = "";
		String LAST_CE = "";
		if(vis == false)
		{
			visitedStates.add(initM);
			for (int i = 0;i < randomooreAutomaton.inputAlphabet.size() && AA == true;i++)
			{

				String A = randomooreAutomaton.transitionTable.get(initM + randomooreAutomaton.inputAlphabet.get(i));
				CE_tmp = CE + randomooreAutomaton.inputAlphabet.get(i);	
				String out1 = FinalDFA(CE_tmp, randomooreAutomaton.initialState);
				String B = getHypothesis.transitionTable.get(initMhat + getHypothesis.inputAlphabet.get(i));
				String out2 = FinalMHAT(CE_tmp,getHypothesis.initialState); 
				visited1.add(initMhat);
				Partition.put(initM, initMhat);
				if(!out1.equals(out2) )
				{
					AA = false;
					break;
				} else {
					
					CE_tmp = DFS(A, B, CE_tmp, visitedStates,getHypothesis);

				}
			}
		
			return CE_tmp;
		} 
		else if  (Partition.containsKey(initM))
		{
			String MhatState = Partition.get(initM);
			if (MhatState != initMhat)
			{
				ArrayList <String> visitedPart = new ArrayList<String> ();
				visitedPart.clear();
				LAST_CE = FinalSearch(initM, initMhat, CE, visitedPart,getHypothesis);
				AAA = true;
			
			}
			
			return LAST_CE; 	
		} else {


		}
		return "";
	}
	
	
private static String FinalSearch(String initialM,String initialMhat,String CE, ArrayList <String> visitedStates1,MooreAutomata getHypothesis)
  {
		
		
		boolean vis2 = visitedStates1.contains(initialM);
		String NCE_tmp = "";
		if(vis2 == false)
		{
			visitedStates1.add(initialM);
			for (int j = 0;j < randomooreAutomaton.transitionTable.size() && AAA == true;j++)
			{
                                       String C = randomooreAutomaton.transitionTable.get(initialM + randomooreAutomaton.transitionTable.get(j));
				       NCE_tmp = CE + randomooreAutomaton.transitionTable.get(j);	
				       String out3 = FinalDFA(NCE_tmp,randomooreAutomaton.initialState);
				       String D = getHypothesis.transitionTable.get(initialMhat + getHypothesis.transitionTable.get(j));
				       String out4 = FinalMHAT(NCE_tmp, getHypothesis.initialState); 
		
				if(out3 != out4)
				{
					AAA = false;
					AA = false;
					break;

				} else {
				
					NCE_tmp = FinalSearch(C,D,NCE_tmp,visitedStates1,getHypothesis);
					
				}
			}
			return NCE_tmp;

		} 
		    AA = false;
		    return "";
	}
}
