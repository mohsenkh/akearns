import java.util.ArrayList;



public class tree extends Binary 
{
	

	
	public static Node root;
	public static ArrayList <String> Prefixes = new ArrayList<String> ();
	public static ArrayList <String> edges = new ArrayList<String> ();
	
	//generate a binary classification tree.
	public static void Tree (String initRoot, String initOut, ArrayList<String> prefixes) 
	{
		        root = new Node(initRoot);
                        ArrayList <String> visited = new ArrayList<String> ();
			ArrayList <String> visitedOUT = new ArrayList<String> ();
			String initialOut = FinalDFA (initOut, randomooreAutomaton.initialState);
	                FDFS(randomooreAutomaton.initialState,initialOut,"",visited,visitedOUT);
		
				for(int ii = 0;ii < Prefixes.size();ii++)
				{
					     String prefix = Prefixes.get(ii);
					     Node pre = new Node(prefix);
					     root.Children.add(pre);
					     root.Children.get(ii).parent = root;
					     root.Edge.add(edges.get(ii));
				
				}
				
		
				
				for(int ll = 0;ll < root.Children.size();ll++)
				{
					prefixes.add(root.Children.get(ll).name);
				}

	 }
	 
            // Sifting operation to find a Node, return String.  
		    public String sift(String str2, Node focusNode)
		    {
		    	
		    	String foundNode = null;
		        if(focusNode != null)
		        {
		            if(focusNode.Children.isEmpty())
		            {
		            		 return focusNode.name;
		     
		            } else {
		            		if(!focusNode.Children.isEmpty())
		            		{
		            			String d = focusNode.name;
		  	    			String sd = str2 + d;
		  	    			String result = FinalDFA(sd, randomooreAutomaton.initialState);
		  	    
		        for(int hh = 0;hh < focusNode.Children.size();hh++)
		        {
		            if (result.equals(focusNode.Edge.get(hh)))
		            {
		            			 focusNode = focusNode.Children.get(hh);
		            			 foundNode = sift(str2,focusNode);
		            	if (foundNode != null)
		            	{
		            			break;
		            	}
		             }
		        }
		            		} else {
		            			
		            		}
		            		
		            	      }
		          } else {
		            		return null;
		            	}
		        return foundNode;
		    }
	 	    
		    // Sifting operation to find a Node, return Node.  
	    public Node siftNode(String str1, Node focusNode) 
	    {
		    	
		    	Node foundNode = null;
		        if(focusNode != null)
		        {
		            if(focusNode.Children.isEmpty())
		            {
		           
		            		 return focusNode;
		     
		            	} else {
		            		if(!focusNode.Children.isEmpty())
		            		{
		            	            String d = focusNode.name;
		  	    	            String sd = str1 + d;
		  	    	            String result = FinalDFA(sd, randomooreAutomaton.initialState);
		            		    for(int hh = 0;hh < focusNode.Children.size();hh++)
		            		    {
		            			     if (result.equals(focusNode.Edge.get(hh)))
		            			     {
		            			         focusNode = focusNode.Children.get(hh);
		            			         foundNode = siftNode(str1,focusNode);
		            		             	 if (foundNode != null){ 
		            			                break;
		            			                                   }
		            	        	 }
		            		 }
		            		} else {
		            			
		            		}
		            		
		            	}
		            	} else {
		            		return null;
		            	}
		        return foundNode;
	    }
	 	  
             // Depth First Search method to find edges and nodes. 
	 		public static void FDFS (String initM,String initOut,String CE, ArrayList <String> visitedStates,ArrayList <String> visitedOut)
	 		{
	 			
	 			String CE_temp = "";
	 			
	 			boolean visOut = visitedOut.contains(initOut);
 				if(visOut == false )
 				{
					visitedOut.add(initOut);
					Prefixes.add(CE);
					edges.add(initOut);
					
				}
	 			
 				boolean vis = visitedStates.contains(initM);
	 			if(vis == false)
	 			{
	 				visitedStates.add(initM);
	 		
	 			for (int i = 0;i < randomooreAutomaton.inputAlphabet.size();i++)
	 			{
	 					
	 					String A = randomooreAutomaton.transitionTable .get(initM + randomooreAutomaton.inputAlphabet.get(i));
	 					CE_temp = CE + randomooreAutomaton.inputAlphabet.get(i);	
	 					String out1 = FinalDFA(CE_temp, randomooreAutomaton.initialState);
	 					if(visitedStates.contains(memberStates)){
	 						break;
	 					} else {
	 				
	 						
	 						FDFS (A, out1, CE_temp, visitedStates, visitedOut);
	 					
	 					        }
	 			}
	 			
	 			} 
	 		
	 		
	 		
	 		}

	 		


	 	  	
}
