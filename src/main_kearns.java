
import java.util.ArrayList;
import java.util.Scanner;




public class main_kearns 
{



	public static ArrayList <String> A = new ArrayList<String> (); // prefixes:new states of hypothesis H.
	public static Integer EQ = 0; // number of equivalence queries.
	public static Integer MQ = 0; // number of membership queries.
        public static MooreAutomata getNewHypothesis = new MooreAutomata(); 

	public static void main(String[] args) 
	{
                String initialRoot = "";
                String initialOut = "";
                String CE = "";
                String counterExample = ""; // number counterexamples.
		Mhat mainHmhat = new Mhat();
		Update mainUpdate = new Update();
		System.out.println("Enter no. of states:");
		Scanner input = new Scanner(System.in);
		int stateNumber;
		stateNumber = input.nextInt();
		System.out.println("Enter no. of Input alphabet:");
		Scanner input1 = new Scanner(System.in);
		int alphabetNumber;
		alphabetNumber = input1.nextInt();
		Binary.mooreRandomGenerator (stateNumber, alphabetNumber); // Enter the number of states and alphabet ( 1 < input alphabet <= 26 , states > 0 )  
	        tree.Tree (initialRoot, initialOut, A);
	
	
	         while( !counterExample.equals(null))
	          {
		
		            MooreAutomata getHypothesis = mainHmhat.HMhat(Binary.randomooreAutomaton.inputAlphabet, A); // create a new hypothesis.
		            getNewHypothesis = getHypothesis;
		            ArrayList <String> visited = new ArrayList<String> ();
	                    counterExample = ADFS.DFS(Binary.randomooreAutomaton.initialState, Binary.initMhatState, CE, visited ,getHypothesis);// counterexample of target M and hypothesis H.
		            ADFS.AA = true;
		            EQ = EQ + 1;
		
		            if(counterExample.equals(""))
		                 {
		    	
			                System.out.println("The M target equals to Mhat :)");
			                break;
			
		                 }
	
			         mainUpdate.UpdateTreeM(counterExample,A); // update the binary classification tree and find a new state of target M. 

	          }

		            System.out.println("# Membership Query:"+ MQ); // number of membership queries in each implementation.
		            System.out.println("# Equevalence Query:"+ EQ); // number of equivalence queries in each implementation.

	}

 }



